<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sarap Web Application</title>

    <!-- Styles -->
    <link href="css/app.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        Sarana dan Prasarana
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Prasarana</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Kategori Barang</a></li>
                                    <li><a href="#">Barang</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Pemilik</a></li>
                            <li><a href="#">Laporan Peminjaman</a></li>
                        <!-- Authentication Links -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Gordon Doni</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Profil Saya</a></li>
                                    <li><a href="#">Keluar</a></li>
                                </ul>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- CONTENT -->
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Tambah Pengguna
                        <a href="pengguna-index.php" class="btn btn-xs btn-default pull-right">Kembali</a>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="">
                            <input type="hidden" name="id">
                            <div class="form-group">
                                <label class="control-label col-md-offset-5 col-xs-offset-5">Tambah Data Pengguna</label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">NISN</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="number" class="form-control" name="nisn" placeholder="0091939765" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Nama</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" class="form-control" name="nama" placeholder="Budi Bagus" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Tanggal Lahir</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="date" class="form-control" name="tgllahir" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Jenis Kelamin</label>
                                  <div class="col-md-8">
                                    <div id="gender" class="btn-group" data-toggle="buttons">
                                      <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="jeniskelamin" value="L"> &nbsp; Laki-Laki &nbsp;
                                      </label>
                                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="jeniskelamin" value="P"> Perempuan
                                      </label>
                                  </div>
                            </div>
                          <br><br>
                          <div class="form-group">
                                <label class="col-md-2 control-label">Kelas</label>
                                  <div class="col-md-8">
                                    <select class="select2_single form-control" tabindex="-1" name="idkelas">
                                      <option></option>
                                      <option>10 TKR</option>
                                      <option>10 TSM</option>
                                      <option>10 TKJ</option>
                                      <option>10 RPL</option>
                                      <option>10 Far</option>
                                      <option>10 Kep</option>
                                      <option>11 TKR</option>
                                      <option>11 TSM</option>
                                      <option>11 TKJ</option>
                                      <option>11 RPL</option>
                                      <option>11 Far</option>
                                      <option>11 Kep</option>
                                      <option>12 TKR</option>
                                      <option>12 TSM</option>
                                      <option>12 TKJ</option>
                                      <option>12 RPL</option>
                                      <option>12 Far</option>
                                      <option>12 Kep</option>
                                    </select>
                                  </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END -->
    </div>

    <!-- Scripts -->
    <script src="js/app.js"></script>
</body>
</html>
<?php
include 'config/database.php';
// menyimpan data kedalam variabel
if(isset($_POST['nisn'])){
    $id     = $_POST['id'];
    $nisn   = $_POST['nisn'];
    $nama   = $_POST['nama'];
    $tgllahir  = $_POST['tgllahir'];
    $jeniskelamin   = $_POST['jeniskelamin'];
    $idkelas   = $_POST['idkelas'];
// query SQL untuk insert data
    $input=$connection->query("insert into pengguna value ('$id','$nisn','$nama','$tgllahir','$jeniskelamin','$idkelas')");
// mengalihkan ke halaman index.php
    if ($input) {
	    echo "<script>  document.location.href='pengguna-index.php';</script>";
    }else{
    	echo "Gagal Memasukan Data!";
    }
}
?>
