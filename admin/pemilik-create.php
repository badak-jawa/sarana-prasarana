<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sarap Web Application</title>

    <!-- Styles -->
    <link href="css/app.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        Sarana dan Prasarana
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Prasarana</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Kategori Barang</a></li>
                                    <li><a href="#">Barang</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Pemilik</a></li>
                            <li><a href="#">Laporan Peminjaman</a></li>
                        <!-- Authentication Links -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Gordon Doni</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Profil Saya</a></li>
                                    <li><a href="#">Keluar</a></li>
                                </ul>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- CONTENT -->
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Tambah Pemilik
                        <a href="pemilik-index.php" class="btn btn-xs btn-default pull-right">Kembali</a>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="">
                            <div class="form-group">
                                <label class="control-label col-md-offset-5 col-xs-offset-5">Tambah Data Pemilik</label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Nama Pemilik</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" class="form-control" name="nama" placeholder="Agus" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END -->
    </div>

    <!-- Scripts -->
    <script src="js/app.js"></script>
</body>
</html>
<?php
include '../config/database.php';
// menyimpan data kedalam variabel
if(isset($_POST['nama'])){
    $nama   = $_POST['nama'];
// query SQL untuk insert data
    $input=$connection->query("INSERT INTO pemilik(nama) VALUES ('$nama')");
// mengalihkan ke halaman index.php
    if ($input) {
	    echo "<script>  document.location.href='pemilik-index.php';</script>";
    }else{
    	echo "Gagal Memasukan Data!";
    }
}
?>
