<?php
    include "config/database.php";
    $execute = $connection->query("select * from kelas");
    $total = mysqli_num_rows($execute);
    $no=0;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sarap Web Application</title>

    <!-- Styles -->
    <link href="css/app.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        Sarana dan Prasarana
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Prasarana</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Kategori Barang</a></li>
                                    <li><a href="#">Barang</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Pemilik</a></li>
                            <li><a href="#">Laporan Peminjaman</a></li>
                        <!-- Authentication Links -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Gordon Doni</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Profil Saya</a></li>
                                    <li><a href="#">Keluar</a></li>
                                </ul>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- CONTENT -->
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Data Kelas
                        <a href="kelas-create.php" class="btn btn-xs btn-default pull-right">Tambah Kelas</a>
                    </div>
                    <div class="panel-body">
                        <div class="text-center">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <input class="form-control" type="text" class="form-control" name="namakelas" placeholder="Cari Berdasarkan Nama Kelas..." value="" autofocus>
                                        <span class="fa fa-search"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <p>Total Jumlah Kelas: <?php echo $total; ?></p>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <td>No</td>
                                    <td>Nama Kelas</td>
                                </thead>
                                <tbody>
                                   <?php

                                        while($isi = mysqli_fetch_array($execute)){
                                            $no++;
                                    ?>
                                    <tr>
                                        <td><?php echo $no;  ?></td>
                                        <td><?php echo $isi['namakelas']; ?></td>
                                        <td align="Right">
                                            <a href="kelas-edit.php?id=<?php echo $isi['id'] ; ?>" class="btn btn-sm btn-warning">Edit </a>
                                            <a onclick="return confirm('Apakah anda yakin ingin menghapusnya?')" href="kelas-delete.php?id=<?php echo $isi['id'] ; ?>" class="btn btn-sm btn-danger">Hapus</a>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END -->
    </div>

    <!-- Scripts -->
    <script src="js/app.js"></script>
</body>
</html>
