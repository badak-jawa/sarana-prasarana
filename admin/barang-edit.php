<?php
include "../config/database.php";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $data = $connection->query("SELECT * FROM barang WHERE id = '$id'");
    $isi = $data->fetch_array(MYSQLI_BOTH);
    $id = $isi['id'];
    $kode = $isi['kode'];
    $idkategori = $isi['idkategori'];
    $pemilik = $isi['pemilik'];
    $deskripsi = $isi['deskripsi'];
    $tanggal_masuk = $isi['tanggal_masuk'];
    $status = $isi['status'];
} else {
    echo "<script>document.location.href='barang-index.php?alert=Barang Tidak Ditemukan';</script>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sarap Web Application</title>

    <!-- Styles -->
    <link href="../css/app.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        Sarana dan Prasarana
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Prasarana</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Kategori Barang</a></li>
                                    <li><a href="#">Barang</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Pemilik</a></li>
                            <li><a href="#">Laporan Peminjaman</a></li>
                        <!-- Authentication Links -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Gordon Doni</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Profil Saya</a></li>
                                    <li><a href="#">Keluar</a></li>
                                </ul>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- CONTENT -->
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Barang
                        <a href="barang-index.php" class="btn btn-xs btn-default pull-right">Kembali</a>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="barang-update.php?id=<?php echo $id; ?>">
                            <div class="form-group">
                                <label class="control-label col-md-offset-5 col-xs-offset-5">Edit Data Barang</label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Kode</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" class="form-control" name="kode" placeholder="" value="<?php echo $kode ?>" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Kategori</label>
                                <div class="col-md-8">
                                    <select class="select2_single form-control" name="idkategori" value="<?php echo $idkategori ?>">
                                        <option>-- Pilih Pemilik --</option>
                                        <?php
                                            include "../config/database.php";
                                            $query = $connection->query("select * from barang_kategori");
							                while ($nama = mysqli_fetch_array($query)) {
								                echo "<option value='".$nama['id']."'> ".$nama['nama']."</option>";
							                 }
						                ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Pemilik</label>
                                <div class="col-md-8">
                                    <select class="select2_single form-control" name="pemilik" value="<?php echo $pemilik ?>">
                                        <option>-- Pilih Pemilik --</option>
                                        <?php
                                            include "../config/database.php";
                                            $query = $connection->query("select * from pemilik");
							                while ($nama = mysqli_fetch_array($query)) {
								                echo "<option value='".$nama['id']."'> ".$nama['nama']."</option>";
							                }
						                ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Deskripsi</label>
                                <div class="col-md-8">
                                    <textarea value="<?php echo $deskripsi ?>" class="form-control" type="date" class="form-control" name="deskripsi" autofocus><?php echo $isi['deskripsi'];?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Tanggal Masuk</label>
                                <div class="col-md-8">
                                    <input value="<?php echo $tanggal_masuk ?>" class="form-control" type="date" class="form-control" name="tanggal_masuk" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Status</label> 
                                <div class="col-sm-8">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status" value="1" checked>
                                        <label class="form-check-label" >Tersedia</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status" value="2" checked>
                                        <label class="form-check-label" >Tidak Tersedia</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status" value="3" checked>
                                        <label class="form-check-label" >Rusak</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status" value="4" checked>
                                        <label class="form-check-label" >Proses Perbaikan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END -->
    </div>

    <!-- Scripts -->
    <script src="../js/app.js"></script>
</body>
</html>
