<?php
include "config/database.php";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $data = $connection->query("SELECT * FROM pengguna WHERE id = '$id'");
    $isi = $data->fetch_array(MYSQLI_BOTH);
    $id = $isi['id'];
    $nisn = $isi['nisn'];
    $nama = $isi['nama'];
    $tgllahir = $isi['tgllahir'];
    $jeniskelamin = $isi['jeniskelamin'];
    $idkelas = $isi['idkelas'];
} else {
    echo "<script>document.location.href='pengguna-index.php?alert=Pengguna Tidak Ditemukan';</script>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sarap Web Application</title>

    <!-- Styles -->
    <link href="css/app.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        Sarana dan Prasarana
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Prasarana</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Kategori Barang</a></li>
                                    <li><a href="#">Barang</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Pemilik</a></li>
                            <li><a href="#">Laporan Peminjaman</a></li>
                        <!-- Authentication Links -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Gordon Doni</a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Profil Saya</a></li>
                                    <li><a href="#">Keluar</a></li>
                                </ul>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- CONTENT -->
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Kategori Pengguna<br>
                        <a href="pengguna-index.php" class="btn btn-xs btn-default pull-right">Kembali</a>
                    </div>
                    <!--NOTE-->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Note: Isi lagi jenis kelaminnya</label>
                    </div>
                  

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="pengguna-update.php?id=<?php echo $id; ?>">
                            <div class="form-group">
                                <label class="control-label col-md-offset-5 col-xs-offset-5">Edit Data Pengguna</label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">NISN</label>
                                <div class="col-md-8">
                                    <readonly><input class="form-control" readonly="readonly" type="number" value="<?php echo $nisn ?>" class="form-control" name="nisn" placeholder="0091939765" autofocus></readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Nama</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" value="<?php echo $nama ?>" class="form-control" name="nama" placeholder="Budi Bagus" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Tanggal Lahir</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="date" value="<?php echo $tgllahir ?>" class="form-control" name="tgllahir" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Jenis Kelamin</label>
                                  <div class="col-md-8">
                                    <div id="gender" class="btn-group" data-toggle="buttons">
                                      <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="jeniskelamin" value="L"> &nbsp; Laki-Laki &nbsp;
                                      </label>
                                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="jeniskelamin" value="P"> Perempuan
                                      </label>
                                  </div>
                            </div>
                          <br><br>
                          <div class="form-group">
                                <label class="col-md-2 control-label">Kelas</label>
                                  <div class="col-md-8">
                                    <select class="select2_single form-control"tabindex="-1" name="idkelas">
                                      <option><?php echo $idkelas?></option>
                                      <option>10</option>

                                      <option>11</option>

                                      <option>12</option>

                                    </select>
                                  </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END -->
    </div>

    <!-- Scripts -->
    <script src="js/app.js"></script>
</body>
</html>
