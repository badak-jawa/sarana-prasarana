<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sarap Web Application</title>

    <!-- Styles -->
    <link href="css/app.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        Sarana dan Prasarana
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                            <li><a href="#">Pinjam Barang</a></li>
                        <!-- Authentication Links -->
                            <li class='active'><a href="#">Login</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--notif-->
        <?php
	         if(isset($_GET['pesan'])){
		          if($_GET['pesan'] == "gagal"){
			           echo "Login gagal! username dan password salah!";
		          }else if($_GET['pesan'] == "logout"){
			           echo "Anda telah berhasil logout";
		          }else if($_GET['pesan'] == "belum_login"){
			           echo "Anda harus login untuk mengakses halaman admin";
		          }
	        }
	?>
        <!-- CONTENT -->
        <div class="container">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Login
                        <a href="#" class="btn btn-xs btn-default pull-right">Kembali</a>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="#">
                            <div class="form-group">
                                <label class="control-label col-md-offset-6 col-xs-offset-5">Login</label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Username</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" class="form-control" name="username" placeholder="Username" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Password</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="password" class="form-control" name="password" placeholder="Password" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Login
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END -->
    </div>

    <!-- Scripts -->
    <script src="js/app.js"></script>
</body>
</html>
<?php
$connection = new mysqli('localhost','root','','gordon');
if (isset($_POST ['username'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $cek_data = $connection->query("SELECT * FROM admin WHERE username='$username' AND password='$password'");
    $result = $cek_data->fetch_array(MYSQLI_BOTH);
    $hasil = $cek_data->num_rows;
    if ($hasil == 1) {
        $_SESSION['username'] = $username;
        $_SESSION['password'] = $password;
        echo "<script>document.location='admin.html';</script>";
    }else{
        echo "
                <script>alert('Username atau Password salah'); document.location='login.php';</script>
            ";
    }
}
?>
